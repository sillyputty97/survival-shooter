﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupParticle : MonoBehaviour {

    public Transform PlayerPickupParticle;

	// Use this for initialization
	void Start () {
        PlayerPickupParticle.GetComponent<ParticleSystem>().enableEmission = false;

	}
	
	// Update is called once per frame
	void OnTriggerEnter() {

        PlayerPickupParticle.GetComponent<ParticleSystem>().enableEmission = true;
        StartCoroutine(stopSparkles());


		
	}

    IEnumerator stopSparkles()
    {
        yield return new WaitForSeconds(.4f);
        PlayerPickupParticle.GetComponent<ParticleSystem>().enableEmission = false;
    }
  
}
